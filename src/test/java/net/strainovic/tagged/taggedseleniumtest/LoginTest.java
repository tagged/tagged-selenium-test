package net.strainovic.tagged.taggedseleniumtest;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static net.strainovic.tagged.taggedseleniumtest.constants.TestConstants.TEST_EMAIL;
import static net.strainovic.tagged.taggedseleniumtest.constants.TestConstants.TEST_PASSWORD;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginTest extends AbstractSeleniumTest {


    @Test
    public void login1FailedWrongPasswordSuccessTest() {
        WebDriver driver = getDriver();
        openApplication("http://localhost:8080");

        login(TEST_EMAIL, "someWrongPassword");

        //Assert login failed
        WebElement wrongEmailOrPass = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.className("auth0-global-message-error")));
        Assert.assertNotNull(wrongEmailOrPass);
    }

    @Test
    public void login2SuccessTest() {
        WebDriver driver = getDriver();

        openApplication("http://localhost:8080");
        login(TEST_EMAIL, TEST_PASSWORD);

        //Assert login success
        WebElement appRoot = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("appRoot")));

        Assert.assertNotNull(appRoot);
    }


}