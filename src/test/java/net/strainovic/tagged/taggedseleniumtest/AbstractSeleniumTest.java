package net.strainovic.tagged.taggedseleniumtest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractSeleniumTest {

    private WebDriver driver;

    @BeforeClass
    public static void setupClass() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver_mac");
    }

    @Before
    public void setUp() {
        ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("--headless");
        driver = new ChromeDriver(chromeOptions);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    protected WebDriver getDriver() {
        return driver;
    }

    protected void login(String email, String password) {
        WebDriver driver = getDriver();
        WebElement emailBox = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.name("email")));
        emailBox.sendKeys(email);

        WebElement passwordBox = driver.findElement(By.name("password"));
        passwordBox.sendKeys(password);

        passwordBox.submit();
    }

    protected void openApplication(String appUrl) {
        getDriver().get(appUrl);
    }
}
